'use strict';

var $ = window.jQuery;

//ON DOCUMENT READY
$(document).ready(function() {

  //SVG for Everybody (ie9+, ...)
  svg4everybody();

  //Home event tabs
    $('.tabs .tabs__item').hide();
    $('.tabs').each(function(){
      var $this = $(this);
      $this.find('.tabs__item:first').show().addClass('active');
      $this.find('tabs__item ul li:first a').addClass('active');
    })
    $('.tabs__nav li a').on('click', function(){
        var $this = $(this);
        var tab_item = $this.parents(".tabs").find('.tabs__item');
        tab_item.removeClass('active');
        $this.addClass('active').parent().siblings().find('a').removeClass('active');
        var currentTab = $this.attr('href');
        tab_item.hide();
        $(currentTab).fadeIn('fast').addClass('active');
        return false;
    });

  //script for autosize textarea
  autosize($('textarea'));

    //Activate mobile menu
    $('.js-menu').on('click', function(){
      $('body').toggleClass('menu-is-active');
    });

    //dropdown on mobile menu
    $('.has-dropdown > a').on('click', function(){
      $(this).parent().stop().toggleClass('is-active').children('ul').stop().slideToggle();
    });


    //print icon triger
    $('.js-print').click(function (e) {
      e.preventDefault();
      window.print();
    });


    //newsletter close
    $('.js-close').click(function(){
        $('.newsletter').fadeOut('slow')
    });

    //Search animation
    // $('.menu-top__lang').click(function(){
    //     $(this).toggleClass('is-active');
    //  });

  //Main dropdown menu
  $('.main-menu__submenu').hover(function(e){
      $(this).stop().toggleClass('hovered').children('div').stop().slideToggle();
   });

  $('.main-menu__submenu__submenu').hover(function() {
      $(this).stop().toggleClass('hovered').children('ul').stop().slideToggle();
  });
  
   //sidebar dropdown
  $('.sidebar__submenu').on('click', function(){
      $(this).stop().toggleClass('is-active').children('ul').stop().slideToggle();
    });

  //footer dropdown
  $('.js-menu-dropdown').on('click', function(){
  $(this).stop().toggleClass('is-active').siblings('ul').stop().slideToggle();
  });  
   
  //fix sidebar at some point and remove fixed position at content bottom
  $(window).scroll(function () {
      var threshold = 170;
      if ($(window).scrollTop() >= threshold)
          $('.sidebar__menu').addClass('fixed');
      else
          $('.sidebar__menu').removeClass('fixed');
      var check = $(".content").height() - $(".sidebar__menu").height()-21;
      if ($(window).scrollTop() >= check)
          $('.sidebar__menu').addClass('bottom');
      else
          $('.sidebar__menu').removeClass('bottom');
  });

   //Activate share animation on blog inner page
  $('.js-share').on("click", function(){
    $(this).parent().toggleClass("is-active");
  });

    

    //owl carousel slider for video on home page
     $('.video-slider__bottom').owlCarousel({
        rtl: true,
        loop:true,
        margin:25,
        items:3,
        nav: true,
        navText: false,
        responsive:{
            0:{
               items: 1
             },
            480:{
                items:2
            },
            600:{
                items:3
            },
            1000:{
                items:3
            }
        }
    });

    //on click play video and hide image - video slider (home page)
     $('.featured-img').on('click', function() {
        $(this).fadeOut(1500).siblings('iframe')[0].src += "&autoplay=1";
        ev.preventDefault();
      });
      //click on small carousel items activates main video above (home page)
      $('.video-slider__bottom .slider__item img').on('click', function() {
        var src = "https://www.youtube.com/embed/" + $(this).attr('data-video-src') + "?rel=0&amp;showinfo=0&autoplay=1";
        $('.video-player iframe').attr('src', src).siblings('.featured-img').fadeOut(1500).siblings('h1');
      });

     // owl carousel slider for inner pages
       $('.slider').owlCarousel({
          rtl: true,
          items:1,
          loop:true,
          margin:10,
          dots: true,
          responsive:{
              0:{
                  items:1,
                  dots:false
              },
              800:{
                  items:1
              }
          }
      });

       //Banking operations page slider
        $(".page-slider").owlCarousel({
          rtl:true,
          items:1,
          loop: true,
          autoplay: true,
          nav: true,
          navText: false,
          mouseDrag: false,
          animateIn: 'fadeIn',
          animateOut: 'fadeOut'
        });

        //Banking operations page slider
        $(".slider-news").owlCarousel({
          rtl:true,
          items:1,
          loop: true,
          autoplay: true,
          autoplayTimeout: 5000,
          smartSpeed: 900,
          nav: true,
          navText: false,
          dots: false
        });

    //Vertical titles - hover over link changes background image (blog post featured image -home page)
    //Vertical titles - top
    $('.v-titles__link').hover(function () {
        var background = "url('" + $(this).attr('data-background') + "')";
        $('.js-bg-change').css('background-image', background)
    });
    //Vertical titles - bottom
    $('.v-titles__link-bottom').hover(function () {
        var background = "url('" + $(this).attr('data-background') + "')";
        $('.js-bg-change-bottom').css('background-image', background)
    });

   //sticky footer trick
    var bumpIt = function() {
          $('html').css('margin-bottom', $('.footer-main').height());
        },
        didResize = false;
         bumpIt();
        $(window).resize(function() {
          didResize = true;
        });
        setInterval(function() {
          if(didResize) {
            didResize = false;
            bumpIt();
          }
        }, 250);

    //Blog filter mixitup plugin
    $('.mixitup').mixItUp();
    
    //datepicker
    $( "#datepicker" ).datepicker({
        showOtherMonths: true,
        dayNamesMin:[ "S", "M", "T", "W", "T", "F", "S" ]
    });

     //currency rate page datepicker
    $( "#curr-datepicker" ).datepicker({
      showOtherMonths: true,
      selectOtherMonths: true
    });

    //image popup for gallery
    $('.js-gallery-popup').magnificPopup({ 
      type: 'image',
      closeBtnInside:true,
      gallery:{enabled:true},
      callbacks: {
        buildControls: function() {
          // re-appends controls inside the main container
          this.contentContainer.append(this.arrowLeft.add(this.arrowRight));
        }
      }
    });

    //video gallery popup
    $('.js-video-popup').magnificPopup({
      // disableOn: 480,
      type: 'iframe',
      mainClass: 'mfp-fade',
      removalDelay: 160,
      preloader: false,
      fixedContentPos: false
    });

  
    var map1 = $("#map1");
   
    var locations1 = [
          ['Tripoli', 32.843261, 13.222625],
          ['Benghazi', 32.093757, 20.087539],
          ['Sirte', 31.190257, 16.570380],
          ['Sebha', 27.009946, 14.448652]
      ];

  
    if (map1.length){
      loadMap(locations1, "map1");
    }

    //custom select element
    $("select").selectOrDie( {
      customClass: "contact-select"
    });


    //fixed header
    $(function(){
     var fixedHeader = 50;
      $(window).scroll(function() {
        var scroll = getCurrentScroll();
          if ( scroll >= fixedHeader ) {
               $('.main-menu').addClass('fixed');
            }
            else {
                $('.main-menu').removeClass('fixed');
            }
      });
    function getCurrentScroll() {
        return window.pageYOffset || document.documentElement.scrollTop;
        }
    });


});

//WINDOW ONLOAD
$(window).load(function() {

  // WINDOW RESIZE
  $(window).on('resize', function() {

  }).trigger('resize');
});




function loadMap(loc, elem) {

     var map = new google.maps.Map(document.getElementById(elem), {
       zoom: 10,
       scrollwheel: false,
       center: new google.maps.LatLng(32.811315, 13.125122),
       mapTypeId: google.maps.MapTypeId.ROADMAP,
       disableDefaultUI: true
     });

     var marker, i;

     for (i = 0; i < loc.length; i++) {
         marker = new google.maps.Marker({
         position: new google.maps.LatLng(loc[i][1], loc[i][2]),
         map: map,
         icon: 'images/map-marker.png'
       });
     }

     //Resize Function
    google.maps.event.addDomListener(window, "resize", function() {
      var center = map.getCenter();
      google.maps.event.trigger(map, "resize");
      map.setCenter(center);
    });
}

